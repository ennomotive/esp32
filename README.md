# ESP32

## Esquema funcional

![Alt text](funcional.png)

## Especificaciones de la placa

![Alt text](placa.png)

## Documentación oficial

[PDF](https://www.espressif.com/sites/default/files/documentation/esp32-c6_datasheet_en.pdf)

[Programación en ESP32](https://docs.espressif.com/projects/esp-idf/en/latest/esp32c6/get-started/index.html)

[ESP32-C6-DevKitC-1](https://docs.espressif.com/projects/espressif-esp-dev-kits/en/latest/esp32c6/esp32-c6-devkitc-1/user_guide.html)

## Código de ejemplo

**HA_on_off_light**	 - Bombilla zigbee

**HA_on_off_switch** - Hub Zigbee y leer datos del DHT22

### Leer datos del DHT22

```C
#include <dht.h>
// Para el sensor DHT22
#define SENSOR_TYPE DHT_TYPE_AM2301


void dht_task(void *pvParameter)
{
    float temperature, humidity;
    // EL GPIO 3 debe estar conectado al DHT22
    if (dht_read_float_data(SENSOR_TYPE, 3, &humidity, &temperature) == ESP_OK)
    {
        ESP_LOGI(TAG, "Temperature: %.2f°C, Humidity: %.2f%%", temperature, humidity);
    }
}
```

## Videos de interés

[ESP-IDF SDK Espressif Curso tutorial en español](https://youtube.com/playlist?list=PL-Hb9zZP9qC65SpXHnTAO0-qV6x5JxCMJ&si=U-2auOosiu8qHV2b)

[Como funciona la placa de pruebas](https://www.youtube.com/watch?v=HAQ8krsZOFo)

[video con sensor y placa](https://www.youtube.com/watch?v=WLBY1VgWgfw)

[Sensor de CO2 Sensirion SCD40 y SCD41 con ESP32](https://www.youtube.com/watch?v=yKOtnSHnm20)

[Sensor SCD40](https://www.youtube.com/watch?v=C0hglFujgZ4)

[Batería ESP32](https://www.youtube.com/watch?v=y3QN1IYQx3Y)

[Medidor de CO2 en tu móvil con ESP32 y Sensirion SCD30 Bluetooth (BLE)](https://www.youtube.com/watch?v=ApDrtFQyP1U)

[Arduino con python](https://www.youtube.com/watch?v=elBtWZ_fOZU&list=PLw0SimokefZ3uWQoRsyf-gKNSs4Td-0k6)

[ESPHome](https://www.youtube.com/watch?v=C0hglFujgZ4)

## Notas

No se recomienda usar hubs para conectar la placa al ordenador.

No se recomienda usar cables malo, de baja calidad. Puede que por el canble, el ordenador no detecte la placa
